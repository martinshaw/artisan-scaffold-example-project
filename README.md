# Installation instruction for package development
1. Execute
   ```shell
   ln -s /Users/martinshaw/Sites/artisan-scaffold /Users/martinshaw/Sites/artisan-scaffold-example-project/package
   ```
   with paths changed appropriately, to link your local in-development copy of the `artisan-scaffold` package to this example project. 
   
2. Execute
    ```shell
    docker run --rm \
        -u "$(id -u):$(id -g)" \
        -v $(pwd):/opt \
        -v $(pwd)/package:/opt/package \
        -w /opt \
        laravelsail/php80-composer:latest \
        composer install --ignore-platform-reqs
    ```
   to install Composer.

3. Execute
    ```shell
   cp .env.example .env
   sail up 
   ```
   to serve the example Laravel application.

   Open a new terminal tab and execute
   ```shell
   sail artisan key:generate
   sail npm install
   sail artisan vendor:publish --tag="config"
   ```

4. *(optional)* Execute
    ```shell
   sail artisan scaffold posts title:slug:required.unique content:textarea:required
   sail artisan migrate 
   ```
   to scaffold an example posts resource.
   
5. *(optional)* Execute
    ```shell
   sail artisan scaffold:revert posts 
   ```
   to revert the scaffold of an example posts resource.
